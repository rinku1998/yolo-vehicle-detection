import cv2
import os

# 使用 XVID 編碼
fourcc = cv2.VideoWriter_fourcc(*'mp4v')

# 建立 VideoWriter 物件，輸出影片至 output.avi
# FPS 值為 20.0，解析度為 640x360
out = cv2.VideoWriter('origin.mp4', fourcc, 3.0, (352, 240))

# 載入圖片
image_stream_dir = 'cctv_images/20201004_221240/' # CCTV圖片資料夾
image_stream_list = os.listdir(image_stream_dir) # 讀取資料夾中所有圖片

sorted_image_list = []

for image in image_stream_list:
    sorted_image_list.append(int(image.split('.')[0]))

sorted_image_list.sort()

# 讀取圖片
for image in sorted_image_list:

    image_filename = str(image) + '.jpg'

    image = cv2.imread(os.path.join(image_stream_dir, image_filename))
    out.write(image)
    